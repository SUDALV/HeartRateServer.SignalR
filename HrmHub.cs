﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HeartRateServer.SignalR
{
    public class HrmHub : Hub
    {
        public void HrmFromClient(int value)
        {
            Clients.Others.SendAsync("HrmFromServer", value);
        }
    }
}
